#python3
#Class : CorruptedMessage
# Method: reconstructMessage
#Params: String, int
#Returns: String
#Method sig: String reconstructMessage(String s, int k)
#Goal: given s and k, return a string consisting of multiple occurences of one letter (if possible a letter in s) that replaces k characters in s
#link http://community.topcoder.com/stat?c=problem_statement&pm=13748

import sys
import string
import re

class CorruptedMessage:
    def reconstructMessage(self, s, k):
        otherLetter  = len(s) - int(k) #must replace k letters, so letter replacement candidate must have len(s) - k occurences 
        ltrCount = {}
        candidateFound = False
        candidateList = list(set(string.ascii_lowercase)-set(s))
        if len(candidateList) != 0:
            candidate = candidateList[0]
        if k != 0:
            for c in s:
                ltrCount[c] = s.count(c)
            for key,val in ltrCount.items():
                if val == otherLetter:
                    candidate = key
                    candidateFound = True
                    break
            s = re.sub(r'.', candidate, s)
        return s

x = CorruptedMessage()
corrupt = sys.argv[1]
replaceNum = sys.argv[2]
print(x.reconstructMessage(corrupt, replaceNum))
